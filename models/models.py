from dateutil.relativedelta import relativedelta
import pytz

from odoo import api, fields, models


class Base(models.AbstractModel):
    _inherit = 'base'

    @api.model
    def _read_group_process_groupby(self, gb, query):
        split = gb.split(':')
        field_name = split[0]
        field_type = self._fields[field_name].type
        gb_function = split[1] if len(split) == 2 else None

        if gb_function in ['fiscalyear', 'fiscalquarter']:
            temporal = field_type in ('date', 'datetime')
            tz_convert = (field_type == 'datetime'
                          and self.env.context.get('tz') in pytz.all_timezones)
            qualified_field = self._inherits_join_calc(self._table, field_name, query)
            if temporal:
                if tz_convert:
                    qualified_field = ("timezone('%s', timezone('UTC',%s))"
                                       % (self.env.context.get('tz', 'UTC'), qualified_field))

                fy_last_month = int(self.env.user.company_id.fiscalyear_last_month)
                if fy_last_month != 12:
                    qualified_field += " - interval '%s month'" % fy_last_month
                date_trunc = gb_function == 'fiscalyear' and 'year' or 'quarter'
                qualified_field = "date_trunc('%s', %s)" % (date_trunc, qualified_field)

                if field_type == 'date':
                    qualified_field += "::TIMESTAMP"
            display_format = gb_function == 'fiscalyear' and 'yyyy' or 'QQQ yyyy'
            interval = (gb_function == 'fiscalyear' and relativedelta(years=1)
                        or relativedelta(months=3))
            return {
                'field': field_name,
                'groupby': gb,
                'type': field_type, 
                'display_format': display_format if temporal else None,
                'interval': interval if temporal else None,
                'tz_convert': tz_convert,
                'qualified_field': qualified_field,
            }
        else:
            return super(Base, self)._read_group_process_groupby(gb, query)

    @api.model
    def _read_group_prepare_data(self, key, value, groupby_dict):
        value = super(Base, self)._read_group_prepare_data(key, value, groupby_dict)
        gb = groupby_dict.get(key)
        if (gb and ('fiscalyear' in gb['groupby'] or 'fiscalquarter' in gb['groupby'])
                and gb['type'] in ('date', 'datetime') and value):
            fy_last_month = int(self.env.user.company_id.fiscalyear_last_month)
            if fy_last_month != 12:
                value += relativedelta(months=fy_last_month)
        return value

    @api.model
    def _read_group_format_result(self, data, annotated_groupbys, groupby, domain):
        data = super(Base, self)._read_group_format_result(data, annotated_groupbys,
                                                           groupby, domain)
        fy_last_month = int(self.env.user.company_id.fiscalyear_last_month)
        if fy_last_month != 12:
            for gb in annotated_groupbys:
                if ('fiscalyear' in gb['groupby'] and data[gb['groupby']]
                        and gb['type'] in ('date', 'datetime')):
                    date_range, label = data[gb['groupby']]
                    data[gb['groupby']] = (date_range, '%s-%d' % (label, int(label)+1))
                if ('fiscalquarter' in gb['groupby'] and data[gb['groupby']]
                        and gb['type'] in ('date', 'datetime')):
                    month_quarter = {
                        fy_last_month: 'Q4',
                        (fy_last_month-1) % 12: 'Q4',
                        (fy_last_month-2) % 12: 'Q4',
                        (fy_last_month-3) % 12: 'Q3',
                        (fy_last_month-4) % 12: 'Q3',
                        (fy_last_month-5) % 12: 'Q3',
                        (fy_last_month-6) % 12: 'Q2',
                        (fy_last_month-7) % 12: 'Q2',
                        (fy_last_month-8) % 12: 'Q2',
                        (fy_last_month-9) % 12: 'Q1',
                        (fy_last_month-10) % 12: 'Q1',
                        (fy_last_month-11) % 12: 'Q1',
                    }
                    month_quarter[12] = month_quarter[0]

                    date_range, label = data[gb['groupby']]
                    quarter_label, year_label = label.split(' ')

                    month = (fields.Date.from_string(date_range[:10])+relativedelta(days=1)).month
                    quarter_label = month_quarter[month]
                    if month > fy_last_month:
                        year_label = '%s-%d' % (year_label, int(year_label)+1)
                    else:
                        year_label = '%d-%s' % (int(year_label)-1, year_label)
                    data[gb['groupby']] = (date_range, '%s %s' % (quarter_label, year_label))
        return data
