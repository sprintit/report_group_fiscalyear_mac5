{
    'name': 'Pivot View - Group by Fiscal Year',
    'version': '13.0.1.0',
    'summary': 'New Group by Fiscal Year and Fiscal Quarter for Date and Datetime in Pivot View',
    'description': """
Pivot View - Group by Fiscal Year
=================================

New group by fiscal year and fiscal quarter for date and datetime in pivot view.


Keywords: Odoo Report Group by Fiscal Year, Odoo Report Group by Financial Year,
Odoo Group by Fiscal Year Report, Odoo Group by Financial Year Report,
Odoo Report Fiscal Year Group by, Odoo Report Financial Year Group by,
Odoo Fiscal Year Group by Report, Odoo Financial Year Group by Report,
Odoo Fiscal Year Report Group by, Odoo Financial Year Report Group by,
Odoo Pivot Group by Fiscal Year, Odoo Pivot Group by Financial Year,
Odoo Group by Fiscal Year Pivot, Odoo Group by Financial Year Pivot,
Odoo Pivot Fiscal Year Group by, Odoo Pivot Financial Year Group by,
Odoo Fiscal Year Group by Pivot, Odoo Financial Year Group by Pivot,
Odoo Fiscal Year Pivot Group by, Odoo Financial Year Pivot Group by
    """,
    'category': 'Tools',
    'author': 'MAC5',
    'contributors': ['MAC5'],
    'website': 'https://apps.odoo.com/apps/modules/browse?author=MAC5',
    'depends': ['account'],
    'data': [],
    'qweb': ['static/src/xml/pivot_view.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images':['static/description/banner.png'],
    'price': 49.99,
    'currency': 'EUR',
    'support': 'mac5_odoo@outlook.com',
    'license': 'OPL-1',
}
